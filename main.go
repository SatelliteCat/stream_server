package main

import (
	"fmt"
	"github.com/gorilla/websocket"
	"log"
	"net/http"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

var clients = make(map[*websocket.Conn]bool)
var broadcast = make(chan Message)

type Message struct {
	messageType int
	message     []byte
	clientConn  *websocket.Conn
}

func homePage(w http.ResponseWriter, r *http.Request) {
	_, _ = fmt.Fprintf(w, "Hello from home page")
}

func reader(conn *websocket.Conn) {
	for {
		messageType, p, err := conn.ReadMessage()
		if err != nil {
			log.Printf("Error: %v -- %v", err, conn.RemoteAddr())
			delete(clients, conn)
			return
		}

		broadcast <- Message{messageType: messageType, message: p, clientConn: conn}
		//if err := conn.WriteMessage(messageType, p); err != nil {
		//	log.Println(err)
		//	return
		//}
	}
}

func handleMessages() {
	for {
		// Grab the next message from the broadcast channel
		msg := <-broadcast
		// Send it out to every client that is currently connected
		for client := range clients {
			if msg.clientConn != client {
				err := client.WriteMessage(msg.messageType, msg.message)
				if err != nil {
					log.Printf("Error: %v -- %v", err, client.RemoteAddr())
					client.Close()
					delete(clients, client)
				}
			}
		}
	}
}

func wsEndpoint(w http.ResponseWriter, r *http.Request) {
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		if ws != nil {
			log.Printf("Error: %v -- %v", err, ws.RemoteAddr())
		} else {
			log.Printf("Error: %v", err)
		}
	}

	log.Println("Client connected ", ws.RemoteAddr())
	clients[ws] = true

	reader(ws)

	defer ws.Close()
}

func setupRoutes() {
	http.HandleFunc("/", homePage)
	http.HandleFunc("/ws", wsEndpoint)
}

func main() {
	log.Println("Server start")

	setupRoutes()
	go handleMessages()

	log.Fatal(http.ListenAndServe(":8000", nil))
}
