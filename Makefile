.PHONY: build
build:
	F:\sdk\go1.14.2\bin\go.exe build -v main.go

.PHONY: test
test:
	F:\sdk\go1.14.2\bin\go.exe test -v -race -timeout 30s ./...

.DEFAULT_GOAL := build